'''
Created on Mar 18, 2016

@author: EIFERKITEDU\griessbaum
'''

import sqlite3
import pandas

class Database():
    
    def __init__(self, db_name):
        self.connection = sqlite3.connect(db_name)
        self.cursor = self.connection.cursor()
        self.table_name = 'journeys'
        
    def initialize(self):
        query = 'CREATE TABLE "journeys" ( \
                            `date`    TEXT, \
                            `origin`    TEXT, \
                            `destination`    TEXT, \
                            `cost`    REAL, \
                            `id`    INTEGER, \
                            `distance`    REAL, \
                            `duration`    TEXT, \
                            PRIMARY KEY(id))'
        self.execute(query)
    
    def commit(self):
        self.connection.commit()
                
    def close(self):
        self.connection.close()
        
    def clear_tab(self):
        query = 'DELETE FROM {table_name}'.format(table_name=self.table_name)
        self.cursor.execute(query)
        self.commit()
        
    def insert_trip(self, date, cost, origin=None, destination=None, duration=None, distance=None):
        query = 'INSERT INTO {table_name} (date, cost,  origin, destination, duration, distance) VALUES (?,?,?,?,?,?);'.format(table_name=self.table_name)
        self.cursor.execute(query, (date, cost, origin, destination, duration, distance))
    
    def update_trip(self):
        print('entry already exists')
        
    def upsert_trip(self, date, cost, origin=None, destination=None, duration=None, distance=None):
        if self.entry_exists(date, cost, origin, destination):
            self.update_trip()
        else:
            self.insert_trip(date, cost, origin, destination, duration, distance) 
    
    def entry_exists(self, date, cost, origin=None, destination=None):
        if origin:
            query = 'SELECT id FROM {table_name} WHERE date=? AND cost=? AND origin=? AND destination=?'.format(table_name=self.table_name)
            self.cursor.execute(query, (date, cost, origin, destination))
        elif cost:
            query = 'SELECT id FROM {table_name} WHERE date=? AND cost=? AND origin IS NULL AND destination IS NULL'.format(table_name=self.table_name)
            self.cursor.execute(query, (date, cost))
        else :
            query = 'SELECT id FROM {table_name} WHERE date=? AND cost IS NULL AND origin IS NULL AND destination IS NULL'.format(table_name=self.table_name)
            self.cursor.execute(query, (date,))
        journey_id = self.cursor.fetchone()
        if journey_id:
            return True
        else :
            return False
        
    def extract_df(self):
        query = 'SELECT * FROM {table_name}'.format(table_name=self.table_name)
        return pandas.read_sql(sql=query, con=self.connection, index_col='date', parse_dates={'date':'%Y-%m-%d'})
