'''
Created on Mar 18, 2016

@author: EIFERKITEDU\griessbaum
'''

import requests
import datetime
import time

class Journey():
    
    def __init__(self, origin, destination):
        self.origin = origin
        self.destination = destination
        self.distance = 0
        self.duration = 0
        self.duration_minutes = 0
        self.ask_google()
    
    def ask_google(self):
        time.sleep(1)
        html = 'http://maps.googleapis.com/maps/api/directions/json?origin={o}&destination={d}&mode=transit'
        self.json = requests.get(html.format(o=self.origin, d=self.destination)).json()
        if self.json['status'] == 'OK':
            self.parse_distance()
            self.parse_duration()
        else :
            print('no google trips for trip from {origin} to {destination}'.format(origin=self.origin, destination=self.destination))
        
    def parse_distance(self):
        distance = self.json['routes'][0]['legs'][0]['distance']['text']
        distance = float(distance.split(' ')[0])
        self.distance = distance
        
    def parse_duration(self):
        duration = self.json['routes'][0]['legs'][0]['duration']['text']
        if len(duration.split(' hour')) > 1 :
            hours = int(duration.split(' hour')[0])
            mins = int(duration.split(' hour')[1].split(' ')[1])
        else :
            hours = 0
            mins = int(duration.split(' ')[0])
        self.duration = datetime.timedelta(hours=hours, minutes=mins)
        self.duration_minutes = hours*60 + mins
        
    def __str__(self):
        pass
