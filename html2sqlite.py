from bs4 import BeautifulSoup
import database
import datetime
import google_journeys

db = database.Database()
db.clear_tab()


def date(row):
    the_date = row.find('td', {'class': 'col3'})
    if the_date:
        the_date = datetime.datetime.strptime(the_date.text, '%d.%m.%Y').date()
    return the_date


def extract_cost(row):
    cost = row.find('td', {'class': 'col5'}).text.replace(',', '.').split(' EUR')[0]
    try:
        cost = float(cost)
    except:
        cost = None
    return cost


def extract_destination(row):
    fromto = str(row.find('td', {'class': 'col8'})).split('<br/>')
    if len(fromto) == 4:
        destination = fromto[1].split(' - ')[1].split(' ')[0].split('(')[0]
    else:
        destination = None
    return destination    


def extract_origin(row):
    fromto = str(row.find('td', {'class': 'col8'})).split('<br/>')
    if len(fromto) == 4:
        origin = fromto[1].split(' - ')[0].split(' ')[0].split('(')[0]
    else :
        origin = None
    return origin
    

with open('DB BAHN - Punkteuebersicht.html', 'br') as inHTML:
    soup = BeautifulSoup(inHTML, 'html.parser', from_encoding='ISO-8859-1')

table = soup.table
table_entries = table.find_all('tr')

for entry in table_entries:
    if date(entry):
        cost = extract_cost(entry)
        origin = extract_origin(entry)
        destination = extract_destination(entry)
        if origin:
            print(origin, destination)
            journey = google_journeys.Journey(origin, destination)
            duration = journey.duration_minutes
            distance = journey.distance
            db.insert_trip(date, cost, origin, destination, str(duration), distance)
        else:
            db.insert_trip(date, cost)
        db.commit()
