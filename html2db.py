from bs4 import BeautifulSoup
import database
import datetime
import google_journeys
import pandas

def extract_date(row):
    date = row.find('td', {'class':'col3'})
    if date :
        date = datetime.datetime.strptime(date.text, '%d.%m.%Y').date()
    return date

def extract_cost(row):
    cost = row.find('td', {'class':'col5'}).text.replace(',','.').split(' EUR')[0]
    try :
        cost = float(cost)
    except:
        cost = None
    return cost

def extract_premiumpoints(row):
    premiumpoints = row.find('td', {'class':'col6'})
    if premiumpoints:
        return int(premiumpoints.text.replace('.',''))
    else :
        return None
    
def extract_endpoints(row, origin_dest):
    fromto = str(row.find('td', {'class':'col8'})).split('<br/>')
    if len(fromto) == 4:
        endpoint = fromto[1].split(' - ')[origin_dest]
        if not endpoint.split(' ')[0] == 'Bad':        
            endpoint = endpoint.split(' ')[0]
        else:
            endpoint = endpoint.split('(')[0]
    else:
        endpoint = None
    return endpoint    

def extract_destination(row):
    return extract_endpoints(row, 1)
    
def extract_origin(row):
    return extract_endpoints(row, 0)

def read_table_from_file(filename):
    with open(filename, 'br') as inHTML :
        soup = BeautifulSoup(inHTML, 'html.parser', from_encoding='ISO-8859-1')
    table = soup.table
    table_rows = table.find_all('tr')
    return table_rows

def table2db(table_rows, db):    
    for row in table_rows:
        date = extract_date(row)
        premiumpoints = extract_premiumpoints(row)
        if date and premiumpoints > 0:    
            cost = extract_cost(row)
            origin = extract_origin(row)
            destination = extract_destination(row)
            if origin:
                print(origin, destination)
                journey = google_journeys.Journey(origin, destination)
                duration = journey.duration_minutes
                distance = journey.distance
                db.upsert_trip(date, cost, origin, destination, str(duration), distance)
            else:
                db.upsert_trip(date, cost)
            db.commit()

if __name__ == '__main__':
    #table_rows = read_table_from_file('2016-07-05_DB BAHN - Punkteübersicht.html')
    table_rows = read_table_from_file('2016-03-18_DB BAHN - Punkteuebersicht.html')
    db = database.Database('bahn.sqlite')    
    table2db(table_rows, db)