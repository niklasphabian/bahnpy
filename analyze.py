'''
Created on Mar 18, 2016

@author: EIFERKITEDU\griessbaum
'''

import database
import matplotlib.pyplot as plt 
import numpy

database = database.Database('bahn.sqlite')
df = database.extract_df()
df['cost_to_dist'] = (df['cost']/df['distance'])
df['speed'] = df['distance']/df['duration']*60
#df.resample('6m', how=sum)['duration'].plot()

def draw_price_over_distance():
    plt.plot(df['distance'], df['ratio'], '.')
    plt.xlabel('distance in km')
    plt.ylabel('cost ratio in EUR/km')
    plt.grid('on')
    plt.show()
    
def draw_hist():    
    filtered_ratio = df['cost_to_dist'][df['cost_to_dist']<1000] ##
    filtered_ratio.hist(bins=10)
    ymax = max(numpy.histogram(filtered_ratio)[0])
    plt.xlabel('cost ratio in EUR/km')
    plt.ylabel('number of occurrences')
    plt.plot([0.3, 0.3], [0, ymax], color='r', linestyle='-', linewidth=2)
    plt.legend(['Pendlerpauschale 2014'])
    plt.grid('on')
    plt.show()
    
def average_speed():
    plt.plot(df['cost_to_dist'], df['speed'], '.')
    plt.xlabel('cost ratio in EUR/km')
    plt.ylabel('average speed in km/h')
    plt.grid('on')
    plt.show()
    
    
    
    
    
#draw_hist()
average_speed()
    